# ADYMA

Projet d'un site intra-net pour l'**Association ADYMA**, qui permet aux membres (utilisateurs connectés) de partager autour des balades proposées par l'administrateur du site.

*Travail effectué en groupe avec Myriam Trillat et Majdeddine Alhafez, sous la direction d'Amélie Baghdiguian.*

---

Le site n'est accessible que pour les membres & administrateur et permet d'accéder aux fonctionnalités suivantes :

- **Pour les membres**
    - Visualiser la liste des balades,
    - Visualiser / imprimer une checklist de départ
    - Ajouter une balade en favoris,
    - Indiquer une balade comme ayant été effectuée
    - Envoyer un commentaire (mail) concernant une balade
    - Modifier son mot de passe
---

- **Pour l'administrateur**
    - Ajouter / modifier / supprimer un membre
    - Ajouter / modifier / supprimer une balade et ses commentaires
    - Ajouter / modifier / supprimer un élément de la checklist de départ
---

- **Les balades** contiennent les informations suivantes :
    - Nom
    - Distance & temps
    - Commune & secteur
    - Carte (*map*)
    - Description
    - Points d'intérêts
    - Point de départ
    - Lieux de pause
    - Commentaires

    
