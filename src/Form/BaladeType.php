<?php

namespace App\Form;

use App\Entity\Balade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BaladeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('titre')
			->add('temps')
			->add('distance')
			->add('commune')
			->add('secteur')
            ->add('duree', TimeType::class, [
				'input'  => 'datetime',
				'widget' => 'choice',
			])
			->add('discription')
			->add('pointDinteret')
            ->add('lieuxDePauses')
			->add('pointDeDepart')
			->add('images', FileType::class,[
                'label' => "Download Image",
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Balade::class,
        ]);
    }
}
