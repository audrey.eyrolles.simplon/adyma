<?php

namespace App\Controller;

use App\Entity\Balade;
use App\Entity\Images;
use App\Form\BaladeType;
use App\Repository\BaladeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/balade")
 */
class BaladeController extends AbstractController
{
    /**
     * @Route("/", name="balade_index", methods={"GET"})
     */
    public function index(BaladeRepository $baladeRepository): Response
    {
        return $this->render('balade/index.html.twig', [
            'balades' => $baladeRepository->findAll(),
        ]);
    }

    /**
	 * @IsGranted("ROLE_MEMBER")
     * @Route("/new", name="balade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $balade = new Balade();
        $form = $this->createForm(BaladeType::class, $balade);
		$form->handleRequest($request);
		//dd($form);

        if ($form->isSubmitted() && $form->isValid()) {

			// On récupère les images transmises
			$images = $form->get('images')->getData();
			//dd($images);
    
			// On boucle sur les images
			foreach($images as $image){
				dd($image);
				// On génère un nouveau nom de fichier
				$fichier = md5(uniqid()).'.'.$image->guessExtension();
				//dd($fichier);
				
				// On copie le fichier dans le dossier uploads
				$image->move(
					$this->getParameter('images_directory'),
					$fichier
				);
				
				// On crée l'image dans la base de données
				$img = new Images();
				$img->setName($fichier);
				$balade->addImage($img);
			}


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($balade);
            $entityManager->flush();

            return $this->redirectToRoute('balade_index');
        }

        return $this->render('balade/new.html.twig', [
            'balade' => $balade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="balade_show", methods={"GET"})
     */
    public function show(Balade $balade): Response
    {
        return $this->render('balade/show.html.twig', [
            'balade' => $balade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="balade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Balade $balade): Response
    {
        $form = $this->createForm(BaladeType::class, $balade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

			// On récupère les images transmises
			$images = $form->get('images')->getData();
    
			// On boucle sur les images
			foreach($images as $image){
				// On génère un nouveau nom de fichier
				$fichier = md5(uniqid()).'.'.$image->guessExtension();
				
				// On copie le fichier dans le dossier uploads
				$image->move(
					$this->getParameter('images_directory'),
					$fichier
				);
				
				// On crée l'image dans la base de données
				$img = new Images();
				$img->setName($fichier);
				$balade->addImage($img);
			}
			

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('balade_index');
        }

        return $this->render('balade/edit.html.twig', [
            'balade' => $balade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="balade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Balade $balade): Response
    {
        if ($this->isCsrfTokenValid('delete'.$balade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($balade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('balade_index');
	}
	
	/**
 	* @Route("/supprime/image/{id}", name="balade_delete_image", methods={"DELETE"})
 	*/
	public function deleteImage(Images $image, Request $request){
		$data = json_decode($request->getContent(), true);

		// On vérifie si le token est valide
		if($this->isCsrfTokenValid('delete'.$image->getId(), $data['_token'])){
			// On récupère le nom de l'image
			$nom = $image->getName();
			// On supprime le fichier
			unlink($this->getParameter('images_directory').'/'.$nom);
	
			// On supprime l'entrée de la base
			$em = $this->getDoctrine()->getManager();
			$em->remove($image);
			$em->flush();
	
			// On répond en json
			return new JsonResponse(['success' => 1]);
		}else{
			return new JsonResponse(['error' => 'Token Invalide'], 400);
		}
	}

}
