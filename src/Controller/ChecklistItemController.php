<?php

namespace App\Controller;

// use Doctrine\Migrations\Configuration\Migration\JsonFile; ???
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChecklistItemController extends AbstractController
{
    /**
     * @Route("/checklist", name="checklist")
     */
    public function readChecklist() // ? -> ": JsonFile"
    {
        return $this->render('pages/checklist.html.twig');
    }

    // /**
    //  * @Route("/api/checklist/update", name"updateChecklist")
    //  */
    // public function updateCheckist()
    // {
    //     echo"update checklist!";
    // }
}
