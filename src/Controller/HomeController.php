<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Balade;

class HomeController extends AbstractController
{

     /**
     * @Route("/baladeID/{id}")
     */
    public function contactsimplon(Balade $balade)
    {
        return $this->render("api/baladeId.html.twig", [
            'balade' => $balade,
        ]);
        
    }

}