<?php

namespace App\Controller;

use App\Entity\Balade;
use App\Entity\Images;
use App\Repository\BaladeRepository;
use SebastianBergmann\Environment\Console;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
* @Route("/api")
*/
class APIBaladeController extends AbstractController
{

	/**
    * @Route("/upload.php", name="upload", methods={"POST"})
    */
    public function upload(Request $request)
    {
		$targetPath = "./uploads/" . basename($_FILES["inpFile"]["name"]);
		move_uploaded_file($_FILES["inpFile"]["tmp_name"], $targetPath);

		dd($targetPath);

			return new Response('ok', 201);
	}

	/**
    * @Route("/", name="baladelist", methods={"GET"})
    */
    public function baladelist(BaladeRepository $baldeRepo)
    {
		return $this->render('api/index.html.twig', [
            'balades' => $baldeRepo->findAll(),
        ]);
	}
	
	/**
    * @Route("/balades/list", name="list", methods={"GET"})
    */
    public function list(BaladeRepository $baldeRepo)
    {		
		// On récupère la liste des articles
		$balades = $baldeRepo->findAll();
		//dd($balades);

		// On spécifie qu'on utilise l'encodeur JSON
		$encoders = [new JsonEncoder()];
	
		// On instancie le "normaliseur" pour convertir la collection en tableau
		$normalizers = [new ObjectNormalizer()];

		// On instancie le convertisseur
		$serializer = new Serializer($normalizers, $encoders);

		// On convertit en json
		$jsonContent = $serializer->serialize($balades, 'json', [
			'circular_reference_handler' => function ($object) {
				return $object->getId();
			}
		]);

		// On instancie la réponse
		$response = new Response($jsonContent);

		// On ajoute l'entête HTTP
		$response->headers->set('Content-Type', 'application/json');
	
		// On envoie la réponse
		return $response;
	}
	
	/**
    * @Route("/balade/lire/{id}", name="lire", methods={"GET"})
    */
    public function getBalade(Balade $balade)
    {	
		// On spécifie qu'on utilise l'encodeur JSON
		$encoders = [new JsonEncoder()];
	
		// On instancie le "normaliseur" pour convertir la collection en tableau
		$normalizers = [new ObjectNormalizer()];

		// On instancie le convertisseur
		$serializer = new Serializer($normalizers, $encoders);

		// On convertit en json
		$jsonContent = $serializer->serialize($balade, 'json', [
			'circular_reference_handler' => function ($object) {
				return $object->getId();
			}
		]);

		// On instancie la réponse
		$response = new Response($jsonContent);

		// On ajoute l'entête HTTP
		$response->headers->set('Content-Type', 'application/json');
	
		// On envoie la réponse
		return $response;
	}
	
	/**
 	* @Route("/balade/add", name="ajout", methods={"POST"})
	 */
	public function addBalade(Request $request)
	{		
		// On vérifie si la requête est une requête Ajax
		//if($request->isXmlHttpRequest()) {
			// On instancie un nouvel article
			$balade = new Balade();
	
			// On décode les données envoyées
			$data = json_decode($request->getContent());
			//dd($data);
	
			// On hydrate l'objet
			$balade->setTitre($data->titre);
			$balade->setCommune($data->commune);
			$balade->setDistance($data->distance);
			$balade->setDuree($data->duree);
			$balade->setSecteur($data->secteur);
			$balade->setDiscription($data->discription);
			$balade->setLieuxDePauses($data->lieuxDePauses);
			$balade->setPointDinteret($data->pointDinteret);
			$balade->setPointDeDepart($data->pointDeDepart);
			

			$image = new Images();
			$image->setName($data->images);
			$balade->addImage($image);
			
	
			// On sauvegarde en base
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($balade);
			$entityManager->flush();
	
			// On retourne la confirmation
			return new Response('ok', 201);
		//}
		//return new Response('Failed', 404);
	}


	/**
 	* @Route("/balade/edite/{id}", name="edit", methods={"PUT"})
	 */
	public function editBalade(?Balade $balade, Request $request)
	{
		// On vérifie si la requête est une requête Ajax
		//if($request->isXmlHttpRequest()) {
	
			// On décode les données envoyées
			$data = json_decode($request->getContent());
	
			// On initialise le code de réponse
			$code = 200;
	
			// Si l'article n'est pas trouvé
			if(!$balade){
				// On instancie un nouvel article
				$balade = new Balade();
				// On change le code de réponse
				$code = 201;
			}
	
			// On hydrate l'objet
			$balade->setTitre($data->titre);
			$balade->setDistance($data->distance);
			//$balade->setDuree($data->duree);
			//$balade->setPointDinteret($data->pointDinteret);
			//$balade->setLieuxDePauses($data->lieuxDePauses);
			//$balade->setPointDeDepart($data->pointDeDepart);
			//$image = $this->getDoctrine()->getRepository(Images::class)->find(4);
			//$balade->addImage($image);
	
			// On sauvegarde en base
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($balade);
			$entityManager->flush();
	
			// On retourne la confirmation
			return new Response('ok', $code);
		//}
		//return new Response('Failed', 404);
	}


	/**
 	* @Route("/balade/remove/{id}", name="supprime", methods={"DELETE"})
	 */
	public function removeBalde(Balade $balade)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($balade);
		$entityManager->flush();
		return new Response('ok');
	}

}