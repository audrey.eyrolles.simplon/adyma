global.$ = global.jQuery = require('jquery');



//----Get method pour la list des balades
fetch(`https://127.0.0.1:8000/api/balades/list`)
	.then((response) => {
		if (response) {
			return response.json().then((res) => {
			   console.log(res);

			   var formattedTime = '';
			   let outputListBalade = '';
			   for (let i = 0; i < res.length; i++) {
					//unixTimestamp = res[i].duree.timestamp; 

					// convert to milliseconds 
					// and then create a new Date object 
					//dateObj = new Date(unixTimestamp * 1000); 

					// Get hours from the timestamp 
					//hours = dateObj.getUTCHours(); 

					// Get minutes part from the timestamp 
					//minutes = dateObj.getUTCMinutes(); 
			 
					// Get seconds part from the timestamp 
					//seconds = dateObj.getUTCSeconds(); 
	  
					//formattedTime += hours.toString().padStart(2, '0') + ':' + 
					//	minutes.toString().padStart(2, '0');

					//console.log(formattedTime);

					outputListBalade += `
                          
							<div class="text-center card w-100">
								<div class="card-body">
									<h5 class="card-title" style="color:#631845">${res[i].titre}</h5>
									<!--<p class="card-text" style="color:#B01377">Distance &nbsp : &nbsp ${res[i].distance}Km &nbsp | &nbsp Temps &nbsp : &nbsp ${formattedTime}</p>-->
									<p class="card-text" style="color:#B01377">Distance &nbsp : &nbsp ${res[i].distance}Km &nbsp | &nbsp Temps &nbsp : &nbsp ${res[i].duree}</p>
									<p class="card-text" style="color:#B01377">Commune &nbsp : &nbsp ${res[i].commune}</p>
									<p class="card-text" style="color:#B01377">Secteur &nbsp : &nbsp ${res[i].secteur}</p>

									<div class="row">

										<div class="col">
										<div><a href="#"><figure><img class="icon__social" src="{{ asset('build/like.png') }}" alt="print" title=""></figure></a>
										</div>
										</div>

										<div class="col">
											<a href="#"><figure><img class="icon__social " src="/build/like.png" alt="like" title="" width="30px"></figure></a>
										</div>

										<div class="col">
											<a href="#"><figure><img class="icon__social " src="{{ asset('build/like.png') }}" alt="location" title=""></figure></a>
										</div>

									</div>

									<a onclick="baladeSelected('${res[i].id}')" id="${res[i].id}" class="btn btn-primary" title="${res[i].titre}" href="#">Détails</a>
									
								</div>
							</div>
					`;

					formattedTime = '';
			   }

			   //$('#showListBalade').html(output);
			   document.getElementById("showListBalade").innerHTML = outputListBalade;

			   
			   let addBalade= "";

			   addBalade=`
			   			<div class="text-center"> <hr style="width: 1280px; margin: 2em;"> </div>
			           
						<div class="text-center">
							<a onclick="ajouterBalade()" id="" class="btn btn-primary" title="" href="#">Ajouter une balade</a>
							<hr> 
							<input type="text" id="titre" name="titre" required minlength="4" maxlength="100" size="10" placeholder="Titre"></input>
							<hr> 
							<input type="text" id="commune" name="commune" minlength="4" maxlength="100" size="10" placeholder="Commune"></input>
							<hr> 
							<input type="text" id="secteur" name="secteur" minlength="4" maxlength="100" size="10" placeholder="Secteur"></input>
							<hr> 
							<input type="time" id="duree" name="duree" size="10" placeholder="Durée"></input>
							
							<hr> 
							<input type="text" id="distance" name="distance" minlength="4" maxlength="100" size="10" placeholder="Distance"></input>
							<hr> 

							<form class="form" id="myForm">
								<input type="file" id="inpFile">
							</form>


							<hr> 
							<input type="text" id="discription" name="discription" minlength="4" maxlength="5000" size="50" placeholder="Discription :"></input>
							<hr> 
							<input type="text" id="lieuxDePauses" name="lieuxDePauses" minlength="4" maxlength="5000" size="50" placeholder="Lieux de pause :"></input>
							<hr>
							<input type="text" id="pointDinteret" name="pointDinteret" minlength="4" maxlength="5000" size="50" placeholder="Points d’intérêt :"></input>
							<hr> 
							<input type="text" id="pointDeDepart" name="pointDeDepart" minlength="4" maxlength="5000" size="50" placeholder="Point de départ :"></input>
							
						</div>
			   `;
			   if (document.getElementById("AdminBalade")) {
				document.getElementById("AdminBalade").innerHTML = addBalade;
			   }
			   
			 

			   // plan de reseaux for detect station
			   function ajouterBalade() {
					let titre = document.getElementById("titre").value;
					console.log(titre);
					let commune = document.getElementById("commune").value;
					let secteur = document.getElementById("secteur").value;
					//console.log(zone);
					let duree = document.getElementById("duree").value;
					console.log(duree);
					let distance = document.getElementById("distance").value;
					//console.log(distance);

					const myForm = document.getElementById("myForm");
					const inpFile = document.getElementById("inpFile");
					console.log( document.getElementById("inpFile").files[0].name);


					// For saving the data in server folder
					//myForm.addEventListener("submit", e => {
						//e.preventDefault();

						const endpoint = "upload.php";
						const formData = new FormData();

						console.log(inpFile.files);

						formData.append("inpFile", inpFile.files[0]);

						fetch(endpoint, {
							method: "post",
							body: formData
						}).catch(console.error);
					//})
					
					
					

					let discription = document.getElementById("discription").value;
					//console.log(discription);
					let lieuxDePauses = document.getElementById("lieuxDePauses").value;
					//console.log(lieuxDePauses);
					let pointDinteret = document.getElementById("pointDinteret").value;
					//console.log(pointDinteret);
					let pointDeDepart = document.getElementById("pointDeDepart").value;
					//console.log(pointDeDepart);
					

					//----Add method fetch  
					const myHeaders = new Headers();
					myHeaders.append("Content-Type", "text/plain");
					const raw = JSON.stringify({"titre": document.getElementById("titre").value, 
												"distance": document.getElementById("distance").value, 
												"duree": document.getElementById("duree").value, 
												"commune": document.getElementById("commune").value, 
												"secteur": document.getElementById("secteur").value,
												"images":document.getElementById("inpFile").files[0].name,
												"discription":document.getElementById("discription").value,
												"lieuxDePauses":document.getElementById("lieuxDePauses").value,
												"pointDinteret":document.getElementById("pointDinteret").value,
												"pointDeDepart":document.getElementById("pointDeDepart").value});
					
					console.log(raw);

					const requestOptions = {
					  method: 'POST',
					  headers: myHeaders,
					  body: raw
					};

					fetch("https://127.0.0.1:8000/api/balade/add", requestOptions)
						.then(response => response.json())
						.then(result => console.log(result))
						.catch(error => console.log('error', error));
				}
				// eslint solve "not used function error"
				window.ajouterBalade = ajouterBalade;

		   
		 

			});
		}
	});


	// plan de reseaux for detect station
    function baladeSelected(id) {
		console.log(id);
		var block = document.getElementById('showListBalade');
		block.style.display = 'none';

		var title = document.getElementById('title');
		title.style.display = 'none';

		if (document.getElementById('AdminBalade') !== null) {
			var AdminBalade = document.getElementById('AdminBalade');
		AdminBalade.style.display = 'none';
		}
		

		//----Get method pour une balade
		fetch(`https://127.0.0.1:8000/api/balade/lire/${id}`)
			.then((response) => {
				if (response) {
					return response.json().then((res) => {
					   console.log(res);
					   //console.log(res.duree.timestamp);
					   console.log(res.images[0].name);

					   let outputUneBalade = '';
					   var formattedTime = '';

					   //unixTimestamp = res.duree.timestamp; 

						// convert to milliseconds 
						// and then create a new Date object 
						//dateObj = new Date(unixTimestamp * 1000); 

						// Get hours from the timestamp 
						//hours = dateObj.getUTCHours(); 

						// Get minutes part from the timestamp 
						//minutes = dateObj.getUTCMinutes(); 
			 
						// Get seconds part from the timestamp 
						//seconds = dateObj.getUTCSeconds(); 
	  
						//formattedTime = hours.toString().padStart(2, '0') + ':' + 
						//	minutes.toString().padStart(2, '0');

					   outputUneBalade += `
					   

					   		<div class="text-center">
								<div>
									<h1 style="color:#631845">${res.titre}</h1>
									
									<!--<p style="color:#B01377">Distance &nbsp : &nbsp ${res.distance}Km &nbsp | &nbsp Temps &nbsp : &nbsp ${formattedTime}</p>-->
									<p style="color:#B01377">Distance &nbsp : &nbsp ${res.distance}Km &nbsp | &nbsp Temps &nbsp : &nbsp ${res.duree}</p>
									<p style="color:#B01377">Commune &nbsp : &nbsp ${res.commune}</p>
									<p style="color:#B01377">Secteur &nbsp : &nbsp ${res.secteur}</p>

									<div class="row">

										<div class="col">
										<div><a href="#"><figure><img class="icon__social" src="{{ asset('build/like.png') }}" alt="print" title=""></figure></a>
										</div>
										</div>

										<div class="col">
											<a href="#"><figure><img class="icon__social " src="/build/like.png" alt="like" title="" width="30px"></figure></a>
										</div>

										<div class="col">
											<a href="#"><figure><img class="icon__social " src="{{ asset('build/like.png') }}" alt="location" title=""></figure></a>
										</div>
										
										<h3>|</h3>

										<div class="col">
											<a onclick="EditeBalade(${res.titre})" href="#"><figure><img class="icon__social " src="assets/images/like.png" alt="edite" title=""></figure></a>
										</div>

										<div class="col">
											<a onclick="deleteBalade(${res.titre})" href="#"><figure><img class="icon__social " src="{{ asset('build/like.png') }}" alt="delete" title=""></figure></a>
										</div>


									</div>

									
									<img src="/uploads/${res.images[0].name}" alt="" width="100%" height="300px" style="margin: 2em;">


									<h5 style="color:#B01377">Discription</h5>
									<p>${res.discription}</p>

									<h5 style="color:#B01377">Points d’intérêt</h5>
									<p>${res.pointDinteret}</p>

									<h5 style="color:#B01377">Points de départ</h5>
									<p>${res.pointDeDepart}</p>

									<h5 style="color:#B01377">Lieux de pause</h5>
									<p>${res.lieuxDePauses}</p>
									
								</div>
							</div>
							`;

					//$('#showListBalade').html(output);
			   		document.getElementById("showUneBalade").innerHTML = outputUneBalade;
					});
				}
			});
	}	
	// eslint solve "not used function error"
	window.baladeSelected = baladeSelected;



function EditeBalade(id) {

}
// eslint solve "not used function error"
window.EditeBalade = EditeBalade;



//----Edite method
/*const myHeaders = new Headers();
myHeaders.append("Content-Type", "text/plain");

const raw = "{\n    \"titre\":\"dsdsdsdw\",\n    \"distance\":22\n}";

const requestOptions = {
  method: 'PUT',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("https://127.0.0.1:8000/api/balade/edite/19", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));*/


  
//----Delete method
/*const raw = "";

const requestOptions = {
  method: 'DELETE',
};

fetch("https://127.0.0.1:8000/api/balade/remove/7", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));*/